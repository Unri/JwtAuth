
# Deno JWT Auth

This project follow the [Node.js Auth Tutorial (JWT)](https://www.youtube.com/playlist?list=PL4cUxeGkcC9iqqESP8335DA5cRFp8loyp) by [The Net Ninja](https://www.youtube.com/c/TheNetNinja), but with some variations.

## Variations

Use:

- [Deno v1.6.0](https://deno.land/) instead of [Node.js](https://nodejs.org/)

- [Aleph.js v0.2.27](https://alephjs.org/) instead of [express.js](http://expressjs.com/)

- [ReactJS](https://reactjs.org/) instead of [ejs](https://ejs.co/)

- [GraphQL](https://graphql.org/) instead of [MongoDB](https://www.mongodb.com/)

and some [other packages](import_map.json).

## Purpose

The purpose of this project was to learn:

- How to create a basic JWT Authentification application

- How to create an web app using [Deno](https://deno.land/) and [Aleph.js](https://alephjs.org/)

## Thanks

Thanks to [The Net Ninja](https://www.youtube.com/c/TheNetNinja) for this well explaned tutorial.
