import {setCookie} from 'cookies';
import {create, verify} from 'djwt';
import {config} from 'dotenv';

const {SECRET} = config();
const MAX_AGE = 3 * 24 * 60 * 60;
const algorithm = 'HS512';

export const createToken = (payload: Record<string, unknown>) => create(
    {alg: algorithm,typ: 'JWT'},
    payload,
    SECRET
);

export const verifyToken = (jwt: string) => verify(jwt, SECRET, algorithm);

export const createCookie = (value: string) => {
    const cookie = {
        name: 'jwt',
        value,
        httpOnly: true,
        maxAge: MAX_AGE * 1000
    };
    const tmp = {headers: new Headers()};
    setCookie(tmp, cookie);
    return tmp.headers.get('Set-Cookie')!;
};

export const deleteCookie = (name: string) => {
    const tmp = {headers: new Headers()};
    setCookie(tmp, {name, value: '', httpOnly: true, expires: new Date()});
    return tmp.headers.get('Set-Cookie')!;
};
