import React, {ComponentType} from 'react'
import {Head, Import, SEO, Viewport, useRouter} from 'aleph'
import {useRestrictAuth} from './hooks/restrict-auth.hook.ts';
import Header from "./components/header.tsx";

export default function App({ Page, pageProps }: { Page: ComponentType<any>, pageProps: any }) {
    const {pathname} = useRouter();
    const display = useRestrictAuth('/login');
    return (
        <>
            <Head>
                <meta charSet="UTF-8" />
                <SEO title={pathname} />
                <Viewport width="device-width" initialScale={1.0} />
                <Import from="../style/index.less" />
            </Head>
            <Header />
            {display && <>
                <Page {...pageProps} />
                <footer>Copyright 2020 Ninja Smoothies</footer>
            </>}
        </>
    );
};
