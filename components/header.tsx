import React from 'react';
import {Link} from 'aleph';
import {useCheckUser} from "../hooks/check-user.hook.ts";

export default function Header() {
    const {user, isLoaded} = useCheckUser();
    return (<nav>
        <h1><Link to="/">Ninja Smoothies</Link></h1>
        {isLoaded && <ul>
            {user
                ? <>
                    <li>{user.email}</li>
                    <li><Link to="/logout">Log out</Link></li>
                </>
                : <>
                    <li><Link to="/login">Log in</Link></li>
                    <li><Link to="/signup" className="btn">Sign up</Link></li>
                </>
            }
        </ul>}
    </nav>);
};
