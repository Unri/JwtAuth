import {useEffect, useState} from 'react';
import {useRouter} from 'aleph';

type UserResponse = {
    id: string;
    email: string;
}

export const useCheckUser = () => {
    const {pathname} = useRouter();
    const [user, setUser] = useState<UserResponse>();
    const [isLoaded, setIsLoaded] = useState(false)
    useEffect(() => {
        fetch('/api/check-user').then(async res => {
            setUser(await res.json() ?? false)
            setIsLoaded(true);
        })
    }, [pathname]);
    return {user, isLoaded};
};
