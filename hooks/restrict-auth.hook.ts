import {useEffect, useState} from 'react';
import {redirect, useRouter} from 'aleph';

const restrictedPath = [
    '/smoothies'
];

export const useRestrictAuth = (loginPathname: string) => {
    const {pagePath} = useRouter();
    const [display, setDisplay] = useState(false);
    useEffect(() => {
        if (restrictedPath.includes(pagePath)) {
            fetch('/api/auth').then(res => res.json()).then(({isAuth}) => {
                if (!isAuth)
                    return redirect(loginPathname, true);
                setDisplay(true);
            });
        }
        setDisplay(false);
    }, [pagePath]);
    return restrictedPath.includes(pagePath) === display;
};
