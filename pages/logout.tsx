import React, {useEffect} from 'react';
import {redirect} from 'aleph';

const LOGOUT_TIMEOUT = 1500;

export default function Logout() {
    useEffect(() => {
        fetch('/api/logout', {
            method: 'POST',
        });
        setTimeout(() => {
            redirect('/', true);
        }, LOGOUT_TIMEOUT)
    }, []);
    return (
        <p>You got logged out, you will be automaticaly redirected.</p>
    );
};
