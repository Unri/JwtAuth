import {Link} from 'aleph';
import React from 'react';

export default function Home() {
    return (
        <header>
            <div className="smoothie">
                <img src="/smoothie.png" alt="" />
            </div>
            <div className="headings">
                <h2>Smoothie Recipes</h2>
                <h3>By Ninjas For Ninjas</h3>
                <Link to="/smoothies" className="btn">View Recipes</Link>
            </div>
        </header>
    );
};
