import React, {FormEvent, useCallback, useState} from 'react';
import {redirect} from 'aleph';

interface ErrorMessages {
    email?: string;
    password?: string;
};

interface FormElement {
    elements: Record<string, {value: string}>;
};

export default function Signup() {
    const [errors, setErrors] = useState<ErrorMessages>({});

    const resetErrors = useCallback(() => {
        setErrors({});
    }, []);

    const onSubmit = async (e: FormEvent<FormElement>) => {
        e.preventDefault();
        resetErrors();
        const form = e.currentTarget;
        const {email, password} = form.elements;
        const response = await fetch('/api/signup', {
            headers: {'Content-Type': 'application/json'},
            method: 'POST',
            body: JSON.stringify({email: email.value, password: password.value})
        }).then(res => res.json());
        if (response.errors)
            setErrors(response.errors);
        else
            redirect('/');
    };

    return (
        <form onSubmit={onSubmit}>
            <h2>Sign up</h2>
            <label htmlFor="email">Email</label>
            <input type="email" name="email" required />
            <div className="email error">{errors.email}</div>

            <label htmlFor="password">Password</label>
            <input type="password" name="password" required />
            <div className="password error">{errors.password}</div>

            <button type="submit">Sign up</button>
        </form>
    );
};