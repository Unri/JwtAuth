import React, {useEffect, useState} from 'react';

type Recipe = {
    title: string;
    ingrediants: string[]
};

export default function Smoothies() {
    const [recipies, setRecipies] = useState<Recipe[]>([]);
    useEffect(() => {
        fetch('/api/smoothies').then(async res => setRecipies(await res.json()));
    }, []);
    return (
        <ul className="recipes">
            {recipies.map(({title, ingrediants}, i) => (
                <li key={i} className="recipe">
                    <img src="/smoothie.png" alt="smoothie recipe icon" />
                    <h4>{title}</h4>
                    <p>{ingrediants.join(', ')}</p>
                </li>
            ))}
        </ul>
    );
};