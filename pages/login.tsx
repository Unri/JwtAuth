import React, {FormEvent, useCallback, useState} from 'react';
import {redirect} from 'aleph';

export default function Login() {
    const [errors, setErrors] = useState({email: '', password: ''});

    const resetErrors = useCallback(() => {
        setErrors({email: '', password: ''});
    }, []);

    const onSubmit = async (e: FormEvent) => {
        e.preventDefault();
        resetErrors();
        const form = e.currentTarget;
        const {email, password} = form.elements;
        const response = await fetch('/api/login', {
            headers: {'Content-Type': 'application/json'},
            method: 'POST',
            body: JSON.stringify({email: email.value, password: password.value})
        }).then(res => res.json());
        if (response.errors)
            setErrors(response.errors);
        else {
            redirect('/');
        }
    }

    return (
        <form onSubmit={onSubmit}>
            <h2>Login</h2>

            <label htmlFor="email">Email</label>
            <input type="text" name="email"  />
            <div className="email error">{errors.email}</div>

            <label htmlFor="password">Password</label>
            <input type="password" name="password"  />
            <div className="password error">{errors.password}</div>

            <button type="submit">login</button>
        </form>
    );
};
