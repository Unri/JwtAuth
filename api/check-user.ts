import {APIRequest} from 'aleph/types';
import {getCookies} from 'cookies';
import {verifyToken} from '../tools/api.utils.ts';
import userModel from '../models/user.ts';

export default async (req: APIRequest) => {
    const {jwt} = getCookies(req);
    if (!jwt)
        return req.status(200).json(null);

    const payload = await verifyToken(jwt);
    const user = await userModel.getById(payload.id as string);
    return req.status(200).json(user);
};
