import {APIRequest} from 'aleph/types';
import {deleteCookie} from '../tools/api.utils.ts';

export default (req: APIRequest) => {
    try {
        req.addHeader('Set-Cookie', deleteCookie('jwt')).status(200).json({status: 'ok'});
    } catch (e) {
        req.status(400).json({errors: e});
    }
}