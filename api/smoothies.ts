import {APIRequest} from 'aleph/types';

const recipies = [
    {
        title: 'Banana Boost',
        ingrediants: ['Banana', 'Vanilla ice cream', 'Milk']
    },
    {
        title: 'Tropical Twist',
        ingrediants: ['Peach', 'Pineapple', 'Apple juice']
    },
    {
        title: 'Protein Packer',
        ingrediants: ['Oats', 'Peanut butter', 'Milk', 'Banana', 'Blueberies']
    },
    {
        title: 'Banana Boost',
        ingrediants: ['Banana', 'Vanilla ice cream', 'Milk']
    },
    {
        title: 'Tropical Twist',
        ingrediants: ['Peach', 'Pineapple', 'Apple juice']
    },
    {
        title: 'Protein Packer',
        ingrediants: ['Oats', 'Peanut butter', 'Milk', 'Banana', 'Blueberies']
    }
];

export default (req: APIRequest) => req.status(200).json(recipies);
