import {APIRequest} from 'aleph/types';
import {createCookie, createToken} from '../tools/api.utils.ts';
import userModel from '../models/user.ts';

export default async (req: APIRequest) => {
    const args = await req.decodeBody("json");
    try {
        const {id} = await userModel.login(args);
        const token = await createToken({id});
        req.addHeader('Set-Cookie', createCookie(token)).status(200).json({id});
    } catch (e) {
        req.status(400).json({errors: e});
    }
};
