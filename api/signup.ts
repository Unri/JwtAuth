import {APIRequest} from 'aleph/types';
import {createCookie, createToken} from '../tools/api.utils.ts';
import userModel from '../models/user.ts';

export default async (req: APIRequest) => {
    if (req.method.toUpperCase() !== 'POST')
        return req.status(404);

    const args = await req.decodeBody("json");
    const errors = userModel.validate(args);

    if (errors)
        return req.status(400).json({errors});

    try {
        const {id} = await userModel.create(args);
        const token = await createToken({id});
        req.addHeader('Set-Cookie', createCookie(token)).status(200).json({id});
    } catch (e) {
        req.status(400).json({errors: e});
    }
};