import {APIRequest} from 'aleph/types';
import {getCookies} from 'cookies';
import {verifyToken} from '../tools/api.utils.ts';

export default async (req: APIRequest) => {
    const {jwt} = getCookies(req);
    return req.status(200).json({isAuth: jwt && !!(await verifyToken(jwt))});
};
