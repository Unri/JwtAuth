import {config} from 'dotenv';

const {API_ENDPOINT} = config();

export const fetchApi = ({query, variables}: {query: string, variables: Record<string, unknown>}) => fetch(API_ENDPOINT, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    },
    body: JSON.stringify({query, variables})
}).then(async res => {
    const {data, errors} = await res.json();
    if (errors) {
        console.error(errors[0].message);
        throw errors;
    }
    return data;
});