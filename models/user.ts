import {isEmail} from 'https://deno.land/x/validate@v0.4.0/src/libs/isEmail.ts';
import {fetchApi} from "./fetchApi.ts";
import * as bcrypt from 'bcrypt';

type User = {
    id: string;
    email: string;
    password: string;
};

const MIN_LEN_PASSWORD = 6;

const errorMessage = {
    invalidEmail: 'Invalid email',
    alreadyUsedEmail: 'This email is already used',
    tooShortpassword: `Minimum password length is ${MIN_LEN_PASSWORD}`,
    notRegisteredEmail: 'This email is not registered',
    incorrectPassword: 'Incorrect Password'
};

const validate = ({email, password}: Omit<User, 'id'>) => {
    const errors: Record<string, string> = {};

    if (!isEmail(email)) errors.email = errorMessage.invalidEmail;
    if (password.length < MIN_LEN_PASSWORD) errors.password = errorMessage.tooShortpassword;

    return Object.keys(errors).length ? errors : false;
};

const create = ({email, password}: Omit<User, 'id'>): Promise<Omit<User, 'password'>> => fetchApi({
    query: `
    mutation ($email: String!, $password: String!) {
        insert_user_one(object: {
            email: $email,
            password: $password
        }) {
            id
            email
        }
    }`,
    variables: {
        email: email.toLowerCase(),
        password: bcrypt.hashSync(password)
    }
})
    .then(data => data.insert_user_one)
    .catch(errors => {
        throw errors.reduce((acc: User, {message}: {message: string}) => {
            if (message.search('email'))
                acc.email = errorMessage.alreadyUsedEmail
            return acc;
        }, {});
    });

const login = ({email, password}: Omit<User, 'id'>): Promise<User> => fetchApi({
    query: `
        query ($email: String!) {
            user(where: {email: {_eq: $email}}) {
                id
                email
                password
            }
        }
    `,
    variables: {email: email.toLowerCase()}
})
    .then(({user}) => {
        if (user.length === 0)
            throw {email: errorMessage.notRegisteredEmail};

        if (bcrypt.compareSync(password, user[0].password)) {
            return user[0];
        }
        throw {password: errorMessage.incorrectPassword}
    });

const getById = (id: string): Promise<Omit<User, 'password'>> => fetchApi({
    query: `
        query ($id: uuid!) {
            user(where: {id: {_eq: $id}}) {
                id
                email
            }
        }
    `,
    variables: {id}
})
    .then(res => res.user[0]);

export default {
    validate,
    create,
    login,
    getById
};
